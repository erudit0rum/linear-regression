import sys

def in_row(check, row):
    return check in row

def spec_in_row(row):
    return in_row('2016', row)

def other_check(row):
    return not ("Survey Edition" in row or \
    "Year(s) Under Review" in row or \
    "CL " in row)

def third_check(row):
    return not ("Country" in row)

def read_column(fl_pth, cntr_idx, dt_idx, dt_type, condition, data_structure):
    with open(fl_pth) as csvfile:
        for row in csvfile:
            parsed_row = row.split(',')
            if condition(parsed_row):
                country = parsed_row[cntr_idx].replace('"','')
                data = parsed_row[dt_idx].replace('"', '')
                if not country in data_structure:
                    data_structure[country] = {}
                data_structure[country][dt_type] = data
    return data_structure

def print_to_csv(data):
    with open('outfile.csv', 'w') as out_file:
        out_file.write("country,freedom_score,democracy_score,social_progress_score\n")
        for key, val in data.items():
            out_file.write(f"{key},{val['freedom_score'] if 'freedom_score' in val else ''},{val['democracy_score'] if 'democracy_score' in val else ''},{val['social_progress_score'] if 'social_progress_score' in val else ''}\n")


out = read_column('relevant_sheets/cato_data.csv.0', 2, 138, 'freedom_score', spec_in_row, {})
out = read_column('relevant_sheets/freedom_house_data.csv.1', 0, 130, 'democracy_score', other_check, out)
out = read_column('relevant_sheets/social_progress_data.csv.3', 0, 2, 'social_progress_score', third_check, out)


print_to_csv(out)




"""
sys.exit(1)
all_countries = read_column('relevant_sheets/cato_data.csv.0', 2, spec_in_row) + \
read_column('relevant_sheets/freedom_house_data.csv.1', 0, other_check) + \
read_column('relevant_sheets/social_progress_data.csv.3', 0, third_check)


all_countries.sort()
count = {}
print(all_countries)
for entry in all_countries:
    if not entry in count:
        count[entry] = 0
    count[entry] += 1



for key, val in count.iteritems():
    if val >= 3:
        print(key)

print('break')

for key, val in count.iteritems():
    if val < 3:
        print(key)
"""
