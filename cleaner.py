import sys

def print_to_csv(infile, outfile):
    with open(infile) as in_file:
        with open(outfile, 'w') as out_file:
            for row in in_file:
                if "country,freedom_score,democracy_score,social_progress_score" in row:
                    print('first line')
                    continue
                row_data = row.split(',')
                try:
                    float(row_data[1])
                    float(row_data[2])
                    float(row_data[3])
                    print('writing to it')
                    out_file.write(row)
                except:
                    print('failure')
                    print(row + ' lacks neccesary info')

print_to_csv('parsed_not_clean.csv', 'cleaned.csv')
