"HISTORICAL FIW STATUS AND RATINGS INFORMATION"
"The following worksheets list all country ratings and statuses since the annual Freedom in the World survey was first compiled to cover the year 1972."

KEY
"Survey Edition refers to the various editions of FIW, except for Jan-Feb 1973 through Jan-Feb 1977, which are from the bimonthly journal Freedom at Issue. The first FIW book-length survey was the 1978 edition."
"PR stands for Political Rights, CL stands for Civil Liberties, and Status refers to the Freedom Status. PR and CL are measured on a one-to-seven scale, with one representing the highest degree of Freedom and seven the lowest."
"F, PF, and NF stand for Free, Partly Free, and Not Free."

METHODOLOGY
"Until 2003, countries and territories whose combined average ratings for PR and CL fell between 1.0 and 2.5 were designated Free; between 3.0 and 5.5 Partly Free, and between 5.5 and 7.0 Not Free."
"Beginning with the ratings for 2003, countries whose combined average ratings fall between 3.0 and 5.0 are Partly Free, and those between 5.5 and 7.0 are Not Free."
"Other methodological changes have been effected periodically. For detailed discussions of each edition's methodology and any changes, please consult the methodology essays published with each edition."

"COUNTRY NOTES AND CLARIFICATIONS"
"Several countries have become independent, separated into two or more countries, or merged with a neighboring state. Scores for such countries are given only for the period of their existence as independent states."

"1. In the 1973 edition, South Africa was rated as White(2,3 Free) and Black(5,6 Not Free)."

"2. For Yugoslavia, ratings from edition 2000 to 2003 were for the country that remained following the departures between 1991 and 1992 of Slovenia, Croatia, Macedonia, and Bosnia-Herzegovina."
"In February 2003, the Yugoslav parliament adopted a constitutional charter establishing the state of Serbia and Montenegro. Beginning with the 2004 edition, Yugoslavia is listed as ""Serbia and Montenegro."""
"The State Union of Serbia and Montenegro dissolved when Montenegro withdrew in June 2006, making Serbia an independent state. Thus, the ratings for Serbia and Montenegro are listed separately beginning with the 2007 edition."
"Kosovo was first listed as a territory beginning in 1993-94 edition. Since the 2010 edition, it is listed as an independent country."

"3. South Sudan was first listed as an independent country in the 2012 edition after officially separating from Sudan in July 2011."

"4. The former Zaire is listed under Congo (Kinshasa), and the former Western Samoa is listed under Samoa."


"TERRITORY NOTES AND CLARIFICATIONS"
"Territories have been added to and eliminated from the survey at various times."

"1. Northern Cyprus was listed as a country in the survey from edition 1982 to edition 1991-92, and has been listed as a territory since the 1992-93 edition of the survey."

"2. In the 1995-96 edition of the survey, West Bank and Gaza Strip were considered as one unit. "
"Beginning with the 1996-97 edition a distinction was made between the Israeli-Occupied Territories and the Palestinian Authority Administered Territories."
"This followed the transfer of administrative authority over portions of the West Bank and Gaza from Israel to the Palestinian Authority with the sighing of the Oslo II agreement in September 1995."
"Beginning with the 2011 edition, the territories are divided along geographical, rather than shifting jurisdictional, lines, with one report for the West Bank and one for the Gaza Strip"

"3. Chechnya was rated separately from the 1998-99 edition through the 2009 edition; since then, developments in this jurisdiction have been addressed in the Russia report."

"3. Puerto Rico was rated separately from the 1975 edition to the 2016 edition; since then, developments in this jurisdiction have been addressed in the United States report."

CONTACT
"For more information, contact research@freedomhouse.org."
