import numpy as np
import matplotlib.pyplot as plt
import random
import csv
import matplotlib.pyplot as plt
from functools import reduce
import random
import math
import csv
import matplotlib.pyplot as plt
import math
from functools import reduce

def read_csv(file_name):
    fil = open(file_name)
    data = []
    for row in fil:
        for data_point in row.split(','):
            if data_point == '':
                continue
            data.append(int(data_point))
    return data

def generate_normal_distribution(count):
    out_string = ''
    number = count
    while number > 0:
        total = 0
        for i in range(1000):
            total += random.randint(1,6)
        out_string += "{0},".format(total)
        number -= 1
    return out_string

def get_mean(data):
    count = len(data)
    agr = reduce(lambda a,b : a+b,data)
    return  agr / count

def get_mode(data):
    count = {}
    for point in data:
        if not point in count:
            count[point] = 0
        count[point] += 1
    top_key = 0
    top_value = 0
    for key, value in count.items():
        if value > top_value:
            top_key = key
            top_value = value
    return top_key

def get_median(data):
    sorted_data = sorted(data)
    lngth = len(sorted_data)
    even = lngth % 2 == 0 
    if even:
        return sorted_data[:int(lngth/2)][-1:][0] + \
        sorted_data[int(lngth/2):][0] / 2
    else:
        return sorted_data[int(lngth/2):][0]

def get_standard_deviation(data_array):
    variance = get_variance(data_array)
    print('var', variance)
    return float(math.sqrt(variance))


def generate_least_squares_line(slope, point, width=3):
    point_1 = [point[0] + width, point[0] - width]
    point_2 = [point[1] + (slope*width), point[1] - (slope*width)]
    return [point_1, point_2]


def get_standard_error(data_array):
    """
    this calculates the standard error on the basis of
    standard deviation and size of a sample passed to the
    method
    """
    standard_dev = get_standard_deviation(data_array)
    sample_size = len(data_array)
    return standard_dev / math.sqrt(sample_size)

def get_variance(data_array):
    mean = get_mean(data_array)
    numerator = 0
    for case in data_array:
        numerator += (case - mean)**2
    return numerator / (len(data_array) - 1)

def series(i, n, expr):
    return sum([expr(k) for k in range(i, n)])


def get_correlation(x, y):
    if len(x) != len(y):
        raise Exception('x and y should have the same number and be matched')
    acum = 0
    x_mean = get_mean(x)
    y_mean = get_mean(y)
    x_sd = get_standard_deviation(x)
    y_sd = get_standard_deviation(y)
    for k in range(0, len(x)):
        print('acum', acum)
        acum += ((x[k] - x_mean)/x_sd) * ((y[k] - y_mean)/y_sd)
    one_over = (1/(len(x) - 1))
    print('one_over', one_over)
    return acum * one_over

def get_least_squares_slope(x, y):
    x_sd = get_standard_deviation(x)
    y_sd = get_standard_deviation(y)
    print('x_sd', x_sd)
    print('y_sd', y_sd)
    corr = get_correlation(x, y)
    print('corr', corr)
    return (y_sd/x_sd) * corr

def get_point_on_least_squares(x, y):
    return [get_mean(x), get_mean(y)]

def plot():
    col1 = 1
    col2 = 3

    with open('cleaned.csv') as data_file:
        x = []
        y = []
        tick_spacing = 1
        axes= plt.axes()
        xmax = ''
        xmin = ''
        ymax = ''
        ymin = ''

        for row in data_file:
            row_data = row.split(',')
            a = float(row_data[col1])
            b = float(row_data[col2])
            if xmax == '' or a > xmax:
                xmax = a
            if xmin == '' or a < xmin:
                xmin = a
            if ymax == '' or b > ymax:
                ymax = b
            if ymin == '' or b < ymin:
                ymin = a
            x.append(a)
            y.append(b)

        lss = get_least_squares_slope(x, y)
        pnt = get_point_on_least_squares(x,y)
        ln = generate_least_squares_line(lss, pnt)
        axes.set_xlim([xmin,xmax])
        axes.set_ylim([ymin,ymax])
        axes.set_xticks([i for i in range(int(xmin), int(xmax))])
        axes.set_yticks([i for i in range(int(ymin), int(ymax)) if i % 10 == 0])
        plt.scatter(x, y)
        plt.plot(ln[0], ln[1])
        plt.xlabel('econ freedom')
        plt.ylabel('social freedom')
        plt.show()

    fig, ax = plt.subplots(1,1)
    ax.plot(x,y)
    ax.xaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))
    plt.show()

def main():
    data = generate_normal_distribution(1000)

    sample_size = 100
    sample_num = 5

    mean_of_means = get_mean(mean_list)

def take_n_samples(data_array, sample_size, number_of_samples):
    sample_list = []
    for i in range(number_of_samples):
        sample = take_sample(data_array, sample_size)
        sample_list.append(sample)
    return sample_list

def take_sample(data_array, sample_size):
    return random.sample(data_array, sample_size)

def generate_normal_distribution(number):
    out_array = []
    while number > 0:
        total = 0
        for i in range(1000):
            total += random.randint(1,6)
        out_array.append(total)
        number -= 1
    return out_array

def graph_normal_data(data_array):
    plt.hist(data_array, 200)
    plt.xlabel('Outcome')
    plt.ylabel('Count')
    plt.show()

plot()
