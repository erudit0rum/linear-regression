import random
import csv
import matplotlib.pyplot as plt
from functools import reduce

def read_csv(file_name):
    fil = open(file_name)
    data = []
    for row in fil:
        for data_point in row.split(','):
            if data_point == '':
                continue
            data.append(int(data_point))
    return data

def generate_normal_distribution(count):
    out_string = ''
    number = count
    while number > 0:
        total = 0
        for i in range(1000):
            total += random.randint(1,6)
        out_string += "{0},".format(total)
        number -= 1
    return out_string

def graph_normal_data(file_name):
    data = read_csv(file_name)
    plt.hist(data, 200)
    plt.xlabel('Outcome')
    plt.ylabel('Count')
    plt.show()

def get_mean(file_name):
    data = read_csv(file_name)
    count = len(data)
    agr = reduce(lambda a,b : a+b,data)
    return  agr / count

def get_mode(file_name):
    data = read_csv(file_name)
    count = {}
    for point in data:
        if not point in count:
            count[point] = 0
        count[point] += 1
    top_key = 0
    top_value = 0
    for key, value in count.items():
        if value > top_value:
            top_key = key
            top_value = value
    return top_key

def get_median(file_name):
    data = read_csv(file_name)
    sorted_data = sorted(data)
    lngth = len(sorted_data)
    even = lngth % 2 == 0 
    if even:
        return sorted_data[:int(lngth/2)][-1:][0] + \
        sorted_data[int(lngth/2):][0] / 2
    else:
        return sorted_data[int(lngth/2):][0]
        



#def main():

"""
fl = open('normal_out_file.csv', 'w+')
normal = generate_normal_distribution(50000)
fl.write(normal)
fl.close()
graph_normal_data('normal_out_file.csv')
"""
print('mean')
print(get_mean('test.csv'))
print('mode')
print(get_mode('test.csv'))
print('median')
print(get_median('test.csv'))
